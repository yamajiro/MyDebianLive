 This is Debian Live set for myself.

In config/package-lists file summary
* common  
  Basic C/C++ GUI programming and basic office environment, and utility to rescue system.
* xfce  
  My favorite DE, XFCE set (some plugin remove).
* superdisk  
  Software about movie, wine, and big development library(e.g. Qt, Boost).
* documents(not maintained.)  
  Only document. Generally document isn't included except this list.
* CDset(not maintained.)  
  List Specialized for CD. 
* devel_cd(not maintained.)  
  Minimal list for building this.

Individually, I deploy follow three patterns.
* Desktop(common+xfce+superdisk )
  Pattern for Desktop. Not only developing, but also viewing movie, listen music can do. 
* Portable(common+xfce+documents)
  Pattern for USB memory. To use without network, This pattern includes documents.
* CDROM (CDset)
  Pattern for burning CD-ROM. This pattern includes result of experiment about reducing storage requirement.

License:
Copyright (c) 2015, yamajiro
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project.
